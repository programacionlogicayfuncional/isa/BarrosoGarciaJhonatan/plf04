(ns plf04.core)

;stringE (se presentó como ejemplo)
(defn string-e-1
  [s]
  (letfn [(g [c m]
            (cond 
              (and (= c \e) (>= (m :c) 3)){:r false :c (inc (m :c))}
              (and (= c \e) (>= (m :c) 0)) {:r true :c (inc (m :c))}
              :else m))
           (f [xs]
              (if (empty? xs)
                {:r false :c 0}
                (g (first xs) (f (rest xs)))))]
    ((f s) :r)))


(string-e-1 "Hello")
(string-e-1 "Heelle")
(string-e-1 "Heelele")
(string-e-1 "Hll")
(string-e-1 "e")
(string-e-1 "")


(defn string-e-2
  [s]
  (letfn[(g [x]
          (and (>= x 1) (<= x 3)))
         (h [x y]
            (if (= x \e) (inc y) y))
         (f [xs acc]
            (if (empty? xs)
              (g acc)
              (f (rest xs)
                 (h (first xs) acc))))]
   (f s 0)))


(string-e-2 "Hello")
(string-e-2 "Heelle")
(string-e-2 "Heelele")
(string-e-2 "Hll")
(string-e-2 "e")
(string-e-2 "")

;Solución de los ejercicios 2-10
;stringTimes

(defn string-times-1
  [s n]
  (letfn [(g [s]
             (if (nil? (first s))
               ""
               (str (first s) (g (rest s)))))
          (f [n]
            (if (zero? n)
              ""
              (str (g s) (f (dec n)))))]
    (f n)))


(string-times-1 (vector \H \i) 2)
(string-times-1 (list \H \i) 3)
(string-times-1 "Hi", 1)
(string-times-1 "Hi", 0)
(string-times-1 "Hi", 5)
(string-times-1 "Oh Boy!", 2)
(string-times-1 "x" 4)
(string-times-1 "", 4)
(string-times-1 "code", 2)
(string-times-1 "code", 3)



(defn string-times-2
  [s n]
  (letfn [(g [s]
             (if (nil? (first s))
               ""
               (str (first s) (g (rest s)))))
          (f [n acc]
            (if (zero? n)
              acc
              (f (dec n) (str acc (g s)))))]
    (f n "")))


(string-times-2 (vector \H \i) 2)
(string-times-2 (list \H \i) 3)
(string-times-2 "Hi", 1)
(string-times-2 "Hi", 0)
(string-times-2 "Hi", 5)
(string-times-2 "Oh Boy!", 2)
(string-times-2 "x", 4)
(string-times-2 "", 4)
(string-times-2 "code", 2)
(string-times-2 "code", 3)


;frontTimes
(defn front-times-1
  [s n]
  (letfn [(g [] (cond
                 (> (count s) 3) (str (first s)(second s)(first (rest(rest s))))
                 (= (count s) 2) (str (first s) (second s))
                 (= (count s) 1) (str (first s))
                :else s))
          (f [n]
            (if (zero? n)
              ""
              (str (g)(f (dec n)))))]
    (f n)))


(front-times-1 (vector \C \h \o \c \o \l \a \t \e), 2)
(front-times-1 (list \C \h \o \c \o \l \a \t \e), 3)
(front-times-1 "Chocolate", 2)
(front-times-1 "Chocolate", 3)
(front-times-1 "Abc", 3)
(front-times-1 "Ab", 4)
(front-times-1 "A", 4)
(front-times-1 "", 4)
(front-times-1 "Abc", 0)


(defn front-times-2
  [s n]
  (letfn [(g [] (cond
                  (> (count s) 3) (str (first s) (second s) (first (rest (rest s))))
                  (= (count s) 2) (str (first s) (second s))
                  (= (count s) 1) (str (first s))
                  :else s))
          (f [n acc]
            (if (zero? n)
              acc
              (f (dec n) (str acc (g)))))]
    (f n "")))


(front-times-2 (vector \C \h \o \c \o \l \a \t \e), 2)
(front-times-2 (list \C \h \o \c \o \l \a \t \e), 3)
(front-times-2 "Chocolate", 2)
(front-times-2 "Chocolate", 3)
(front-times-2 "Abc", 3)
(front-times-2 "Ab", 4)
(front-times-2 "A", 4)
(front-times-2 "", 4)
(front-times-2 "Abc", 0)


;countXX

(defn count-xx-1
  [s]
  (letfn [(g [c1 c2 m] ;c1 y c2 son caracteres
            (cond
              (and (= c1 c2) (= c1 \x)) {:c (inc (m :c))}
              :else m))
          (f [xs]
            (if (empty? xs)
              {:c 0}
              (g (first xs) (second xs) (f (rest xs)))))]
    ((f s) :c)))


(count-xx-1 "abcdxx")
(count-xx-1 "xxx")
(count-xx-1 "xxxx")
(count-xx-1 "abc")
(count-xx-1 "Hello there")
(count-xx-1 "Hexxo thxxe")
(count-xx-1 "")
(count-xx-1 "Kittens")
(count-xx-1 "Kittensxxx")


(defn count-xx-2 [s]
  (letfn [(f [ys a]
            (if (empty? ys)
              a
              (f (rest ys) (+ a (case (str (first ys) (second ys))
                                  "xx" 1
                                  0)))))]
    (f s 0)))


(count-xx-2 "abcdxx")
(count-xx-2 "xxx")
(count-xx-2 "xxxx")
(count-xx-2 "abc")
(count-xx-2 "Hello there")
(count-xx-2 "Hexxo thxxe")
(count-xx-2 "")
(count-xx-2 "Kittens")
(count-xx-2 "Kittensxxx")


;stringSplosion

(defn string-splosion-1
  [s]
  (letfn [(g [s n]
             (if (= n 0)
               ""
               (str (first s) (g (rest s) (dec n)))))
          (f [i]
            (if (> i (count s))
              ""
              (str (g s i) (f  (inc i)))))]
    (f 1)))


(string-splosion-1 (vector \h \o \l \a))
(string-splosion-1 "abdc")
(string-splosion-1 "Code")
(string-splosion-1 "abc")
(string-splosion-1 "ab")
(string-splosion-1 "x")
(string-splosion-1 "fade")
(string-splosion-1 "There")
(string-splosion-1 "Kitten")
(string-splosion-1 "Bye")
(string-splosion-1 "Good")
(string-splosion-1 "Bad")


(defn string-splosion-2
  [s]
  (letfn [(g [s n]
            (if (= n 0)
              ""
              (str (first s) (g (rest s) (dec n)))))
          (f [i acc]
            (if (> i (count s))
              acc
              (f (inc i) (str acc (g s i) ))))]
    (f 1 "")))

(string-splosion-1 (vector \h \o \l \a))
(string-splosion-1 "abdc")
(string-splosion-2 "Code")
(string-splosion-2 "abc")
(string-splosion-2 "ab")
(string-splosion-2 "x")
(string-splosion-2 "fade")
(string-splosion-2 "There")
(string-splosion-2 "Kitten")
(string-splosion-2 "Bye")
(string-splosion-2 "Good")
(string-splosion-2 "Bad")


;array123

(defn array-123-1
  [xs]
  (letfn [(g [x y z m]
            (cond
              (and (= x 1) (= y 2) (= z 3)) {:r true :c (inc (m :c))}
              :else m))
          (f [xs]
            (if (empty? xs)
              {:r false :c 0}
              (g (first xs) (second xs) (first (rest (rest xs))) (f (rest xs)))))]
    ((f xs) :r)))


(array-123-1 [1 1 1 1 1 1])
(array-123-1 [1 2 3 1 2 3 1 2 3])
(array-123-1 [1 3 2 1 3 2])
(array-123-1 [1 1 2 3 1])
(array-123-1 [1 1 2 4 1])
(array-123-1 [1 1 2 1 2 3])
(array-123-1 [1 1 2 1 2 1])
(array-123-1 [1 2 3 1 2 3])
(array-123-1 [1 2 3])
(array-123-1 [1 1 1])
(array-123-1 [1 2])
(array-123-1 [1])
(array-123-1 [])


(defn array-123-2
  [s]
  (letfn [(g [x]
            (> x 0))
          (h [x y z a]
            (if (and (= x 1) (= y 2) (= z 3)) (inc a) a))
          (f [xs acc]
            (if (empty? xs)
              (g acc)
              (f (rest xs)
                 (h (first xs) (second xs) (first (rest (rest xs))) acc))))]
    (f s 0)))


(array-123-2 [1 1 1 1 1 1])
(array-123-2 (list 1 2 3 1 2 3 1 2 3))
(array-123-2 [1 3 2 1 3 2])
(array-123-2 [1 1 2 3 1])
(array-123-2 [1 1 2 4 1])
(array-123-2 [1 1 2 1 2 3])
(array-123-2 [1 1 2 1 2 1])
(array-123-2 [1 2 3 1 2 3])
(array-123-2 [1 2 3])
(array-123-2 [1 1 1])
(array-123-2 [1 2])
(array-123-2 [1])
(array-123-2 [])


;stringX

(defn string-x-1
  [s]
  (letfn [(g [c m]
            (cond
              (or (= (m :c) 0) (= (m :c) (dec (count s)))) {:r (str c (m :r) ) :c (inc (m :c))}
              (= c \x) {:r (m :r) :c (inc (m :c))}
              :else {:r (str c (m :r) ) :c (inc (m :c))}))
          (f [xs]
            (if (empty? xs)
              {:r "" :c 0}
              (g (first xs) (f (rest xs)))))]
    ((f s) :r)))


(string-x-1 (vector \a \b \c \d \e \x \x))
(string-x-1 "abcdexx")
(string-x-1 "cdxxxxxxxxmx")
(string-x-1 "xxHxix")
(string-x-1 "abxxxcd")
(string-x-1 "xabxxxcdx")
(string-x-1 "xKittenx")
(string-x-1 "Hello")
(string-x-1 "xx")
(string-x-1 "x")
(string-x-1 "")


(defn string-x-2
  [s]
  (letfn [(g [c] (cond (= c \x) ""
                       :else c))
          (h [xs ys] (cond (> (count s) 2) (str (first xs) ys (last xs))
                           :else xs))
          (f [s acc]
            (if (or (empty? s) (= (count s) 1))
              acc
              (f (rest s) (str acc (g (first s))))))]
    (h s (f (rest s) ""))))


(string-x-1 (vector \a \b \c \d \e \x \x))
(string-x-2 "abcdexx")
(string-x-2 "cdxxxxxxxxmx")
(string-x-2 "xxHxix")
(string-x-2 "abxxxcd")
(string-x-2 "xabxxxcdx")
(string-x-2 "xKittenx")
(string-x-2 "Hello")
(string-x-2 "xx")
(string-x-2 "x")
(string-x-2 "")


; altPairs
(defn alt-pairs-1
  [s]
  (letfn [(f [s i]
            (if (> i (count s))
              ""
              (str (get s i) (get s (+ i 1)) (f s (+ i 4)))))]
    (f s 0)))


(alt-pairs-1 "kitten")
(alt-pairs-1 "Chocolate")
(alt-pairs-1 "CodingHorror")
(alt-pairs-1 "yak")
(alt-pairs-1 "ya")
(alt-pairs-1 "y")
(alt-pairs-1 "")
(alt-pairs-1 "ThisThatTheOther")


(defn alt-pairs-2
  [s]
  (letfn [(f [s i acc]
            (if (> i (count s))
              acc
              (f s (+ i 4) (str acc (get s i) (get s (+ i 1))))))]
    (f s 0 "")))


(alt-pairs-2 "kitten")
(alt-pairs-2 "Chocolate")
(alt-pairs-2 "CodingHorror")
(alt-pairs-2 "yak")
(alt-pairs-2 "ya")
(alt-pairs-2 "y")
(alt-pairs-2 "")
(alt-pairs-2 "ThisThatTheOther")


;stringYak

(defn string-yack-1
  [s]
  (letfn [(g [m]
            (cond
              (and (= (get s (m :c)) \y) (= (get s (+ 2 (m :c))) \k)) {:r (str (m :r)) :c (+ (m :c) 3)}
               :else {:r (str (m :r) (get s (m :c)) ) :c (inc (m :c))}))
          (f [xs]
            (if (empty? xs)
              {:r "" :c 0}
              (g (f (rest xs)))))]
    ((f s) :r)))


(string-yack-1 (vector \y \a \k \p \a \k))
(string-yack-1 "yakpak")
(string-yack-1 "pakyak")
(string-yack-1 "yak123ya")
(string-yack-1 "yak")
(string-yack-1 "yakxxxyak")
(string-yack-1 "HiyakHi")
(string-yack-1 "xxxyakyyyakzzz")


(defn string-yack-2
  [s]
  (letfn [(f [s i]
            (if (> i (count s))
              ""
              (if (and (< (+ i 2) (count s)) (= (get s i) \y) (= (get s (+ i 2)) \k))
                (f s (+ i 3))
                (str (get s i) (f s (inc i))))))]
    (f s 0)))


(string-yack-2 "yakpak")
(string-yack-2 "pakyak")
(string-yack-2 "yak123ya")
(string-yack-2 "yak")
(string-yack-2 "yakxxxyak")
(string-yack-2 "HiyakHi")
(string-yack-2 "xxxyakyyyakzzz")


;has271
(defn has-271-1
  [xs]
  (letfn [        
          (g [x y z m]
            (cond
              (and (= y (+ x 5)) (<= (h (- z (- x 1))) 2)) {:r true :c (inc (m :c))}
              :else m))
          (h [x] (cond
                   (< x 0) (* -1 x)
             :else x))
          (f [xs]
            (if (empty? xs)
              {:r false :c 0}
              (g (first xs) (second xs) (first (rest (rest xs))) (f (rest xs)))))]
    ((f xs) :r)))


(has-271-1 [1 2 7 1])
(has-271-1 [1 2 8 1])
(has-271-1 [2 7 1])
(has-271-1 [3 8 2])
(has-271-1 [2 7 3])
(has-271-1 [2 7 4])
(has-271-1 [2 7 -1])
(has-271-1 [2 7 -2])
(has-271-1 [4 5 3 8 0])
(has-271-1 [2 7 5 10 4])
(has-271-1 [2 7 -2 4 9 3])
(has-271-1 [2 7 5 10 1])
(has-271-1 [2 7 -2 4 10 2])
(has-271-1 [1 1 4 9 0])
(has-271-1 [1 1 4 9 4 9 2])


(defn has-271-2
  [xs]
  (letfn [(g [x]
            (> x 0))
          (h [x y z a]
            (if (and (= y (+ x 5)) (<= (k (- z (- x 1))) 2)) (inc a) a))
          (k [x] (cond
                   (< x 0) (* -1 x)
                   :else x))
          (f [xs acc]
            (if (empty? xs)
              (g acc)
              (f (rest xs)
                 (h (first xs) (second xs) (first (rest (rest xs))) acc))))]
    (f xs 0)))

(has-271-2 [1 2 7 1])
(has-271-2 [1 2 8 1])
(has-271-2 [2 7 1])
(has-271-2 [3 8 2])
(has-271-2 [2 7 3])
(has-271-2 [2 7 4])
(has-271-2 [2 7 -1])
(has-271-2 [2 7 -2])
(has-271-2 [4 5 3 8 0])
(has-271-2 [2 7 5 10 4])
(has-271-2 [2 7 -2 4 9 3])
(has-271-2 [2 7 5 10 1])
(has-271-2 [2 7 -2 4 10 2])
(has-271-2 [1 1 4 9 0])
(has-271-2 [1 1 4 9 4 9 2])